import java.util.HashSet;
import java.util.HashMap;
import java.io.FileReader;
import java.util.ArrayList;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;


public class TextHandler {
	private HashSet<Character> characters = new HashSet<Character>();
	private HashMap<Character, Integer> characterstatistics = new HashMap<Character, Integer>();
	private HashSet<Character> numbers = new HashSet<Character>();
	private HashMap<Character, Integer> numberstatistics = new HashMap<Character, Integer>();
	private HashSet<Character> specialsymbols = new HashSet<Character>();
	private HashMap<Character, Integer> specialsymbolstatistics = new HashMap<Character, Integer>();

	private StringBuffer word = new StringBuffer();
	private HashSet<String> words = new HashSet<String>();
	private HashMap<String, Integer> wordstatistics = new HashMap<String, Integer>(); 

	private String filename;
	private int quantity = 0;

	public TextHandler( String filename) {
		this.filename = filename;
	}

	private int getQuantity() {
		return quantity;
	}

	private HashSet<Character> getCharacters() {
		return characters;
	}

	private HashMap<Character, Integer> getCharacterStatictics() {
		return characterstatistics;
	}

	private HashSet<Character> getNumbers() {
		return numbers;
	}

	private HashMap<Character, Integer> getNumberStatistics() {
		return numberstatistics;
	}

	private HashSet<Character> getSpecialSymbols() {
		return specialsymbols;
	}

	private HashMap<Character, Integer> getSpecialSymbolsStatistic() {
		return specialsymbolstatistics;
	}

	private HashSet<String> getWords() {
		return words;
	}

	private HashMap<String, Integer> getWordsStatistic() {
		return wordstatistics;
	}

	private void insert( char character, HashSet<Character> set) {
		set.add(character);
	}

	private void insert( char character, HashMap<Character, Integer> map) {
		if (map.get(character) == null ) {
			map.put(character, 1);
		} else {
			map.put(character, map.get(character) + 1);		
		}
	}

	private void insert(char character, StringBuffer list) {
		list.append(character);
	}

	private void insert( String word, HashSet<String> set) {
		System.out.println(set);
		set.add(word);
		// System.out.println(set);
	}

	private void insert( String word, HashMap<String, Integer> map) {
		if (map.get(word) == null ) {
			map.put(word, 1);
		} else {
			map.put(word, map.get(word) + 1);		
		}
	}


	private boolean isLetter( int character ) {
		return ( character >= (int) 'A' && character <= (int) 'Z' ) || ( character >= (int) 'a' && character <= (int) 'z' );
	}

	private boolean isNumber(int character) {
    	return character >= (int) '0' && character <= (int) '9';
	}

	private boolean isSpecial(int character) {
    	boolean special = false;

		if ( ( character > (int) ' ' && character < (int) '0' ) || ( character > (int) '9' && character < (int) 'A' ) ) {
		    special = true;
		}
		if ( ( character > (int) 'Z' && character < (int) 'a' ) || ( character > (int) 'z' && character <=(int)  '~' ) ) {
		    special = true;
		}
		return special;
}



	public void parseText() throws IOException {
		FileReader inputStream = null;
		int diff = (int) 'a' - (int) 'A';

		try {
        	inputStream = new FileReader("symbols.txt");

        	int character;
        	while ((character = inputStream.read()) != -1) {
         		if ( isLetter(character) ) {
         			quantity += 1;
         			if (character < (int) 'a') {
         				character += diff;
         			}

         			insert((char) character, characters);
         			insert((char) character, characterstatistics);

         			insert((char) character, word);
         			// System.out.println(word);
         		}
         		if( isNumber(character) ) {
         			insert((char) character, numbers);
            		insert((char) character, numberstatistics);
            		quantity += 1;
         		}   
				if ( isSpecial(character) ) {
            		insert((char) character, specialsymbols);
            		insert((char) character, specialsymbolstatistics);
            		quantity += 1;
            		if( character == '\'' ) {
                		insert((char) character, word);
            		}	
        		}
        		System.out.println(character);
        		if( character != (int) '\'' && (isSpecial(character) || character == (int) ' ')) {
        			if ( word.length() != 0 ) {
        				insert(word.toString(), words);
        				insert(word.toString(), wordstatistics);
        			}
        			word.setLength(0);
        		}	
        	}
    	} finally {
        	if (inputStream != null) {
            	inputStream.close();
        	}
		}
	}
	public String toString() {
		StringBuffer characterstatistic = new StringBuffer();
		Iterator it = characterstatistics.entrySet().iterator();

		while (it.hasNext()) {
        	Map.Entry pairs = (Map.Entry)it.next();
        	characterstatistic.append(pairs.getKey() + " = " + pairs.getValue() + '\n');
        	it.remove();
    	}

		return "Symbols found: " + getQuantity() + '\n' + "Letters statistic:" + '\n' + "Unique: " + getCharacters() 
				+ '\n' + "Matches in text: " + '\n' + characterstatistic + '\n' + "Numbers statistic:" + '\n' + "Unique: "
				+ getNumbers() + '\n' + "Matches in text: " + '\n' + getNumberStatistics()
				+ "Special symbols statistic:" + '\n' + "Unique: " + getSpecialSymbols() + '\n'
				+ "Matches in text: " + getSpecialSymbolsStatistic() + '\n' + "Words statistics:" + '\n' + "Unique words: " 
				+ getWords() + '\n' + "Matches in text: " + getWordsStatistic();
	}
}
