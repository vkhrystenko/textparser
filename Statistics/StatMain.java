import java.io.IOException;

public class StatMain {
    public static void main(String[] args)
     throws IOException
    {
     TextHandler handler = new TextHandler("symbols.txt");

     handler.parseText();

     System.out.println(handler);
     // System.out.println("Done...");
    }
}
